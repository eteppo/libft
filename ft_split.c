/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_split.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 10:33:46 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/31 13:57:46 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**free_array(char **array)
{
	int	i;

	i = 0;
	while (array && array[i])
	{
		free(array[i]);
		i++;
	}
	if (array)
		free(array);
	array = 0;
	return (0);
}

static size_t	str_counter(char const *s, char c)
{
	size_t	i;
	size_t	counter;

	i = 0;
	counter = 0;
	if (s[0] != c)
		counter++;
	while (i < ft_strlen(s))
	{
		if (s[i] == c && s[i + 1] != c && s[i + 1] != '\0')
			counter++;
		i++;
	}
	return (counter);
}

static char	**malloc_array(size_t count, char const *s, char c)
{
	size_t	word_i;
	size_t	char_i;
	size_t	substr_len;
	char	**array;

	word_i = 0;
	char_i = 0;
	array = malloc(sizeof(char *) * (count + 1));
	if (!array)
		return (0);
	while (word_i < count)
	{
		substr_len = 0;
		while (s[char_i] == c && s[char_i] != '\0')
			char_i++;
		while (s[char_i + substr_len] != c && s[char_i + substr_len] != '\0')
			substr_len++;
		char_i += substr_len;
		array[word_i] = malloc(sizeof(size_t) * (substr_len + 1));
		if (!array[word_i])
			return (free_array(array));
		word_i++;
	}
	return (array);
}

static char	**fill_array(char const *s, char **array, char c)
{
	size_t	i;
	size_t	j;
	size_t	k;

	i = 0;
	k = 0;
	while (k < ft_strlen(s))
	{
		j = 0;
		while (s[k] == c && s[k] != '\0')
			k++;
		while (s[k] != c && s[k] != '\0')
		{
			array[i][j] = s[k];
			j++;
			k++;
			if (s[k] == c || s[k] == '\0')
				array[i][j] = '\0';
		}
		i++;
		k++;
	}
	array[str_counter(s, c)] = 0;
	return (array);
}

char	**ft_split(char const *s, char c)
{
	char	**array;
	size_t	count;

	if (!s)
		return (0);
	if (ft_strlen(s) == 0)
	{
		array = malloc(sizeof(char *));
		if (!array)
			return (0);
		array[0] = 0;
		return (array);
	}
	count = str_counter(s, c);
	array = malloc_array(count, s, c);
	if (!array)
		return (0);
	fill_array(s, array, c);
	return (array);
}
