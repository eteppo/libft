/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_atof_with_error.c                               :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/15 12:11:41 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/09 12:16:05 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	contains_digits(char *str)
{
	int	i;

	i = 0;
	if (str)
	{
		while (str[i])
		{
			if (ft_isdigit(str[i]) == 1)
				return (1);
			i++;
		}
	}
	return (0);
}

float	atof_continued(char *str, double res, int neg, int *error)
{
	double	dec;
	int		i;

	dec = 0.1;
	i = 0;
	*error = -1;
	if (str[i] == '.')
	{
		i++;
		while (ft_isdigit(str[i]))
		{
			res += (str[i] - '0') * dec;
			if (res > FLT_MAX || res * neg < -FLT_MAX)
				return (0);
			dec /= 10;
			i++;
		}
	}
	if (str[i] != '\0')
		return (0);
	res *= neg;
	*error = 0;
	return (res);
}

float	ft_atof_with_error(char *str, int *error)
{
	int		i;
	double	res;
	int		neg;

	res = 0;
	neg = 1;
	i = 0;
	*error = -1;
	if (contains_digits(str) == 0)
		return (0);
	while (str[i] == ' ')
		i++;
	if (str[i] == '-')
		neg = -1;
	if (str[i] == '-')
		i++;
	while (ft_isdigit(str[i]))
	{
		res = res * 10 + str[i] - '0';
		if (res > FLT_MAX || (res * neg) < -FLT_MAX)
			return (0);
		i++;
	}
	return (atof_continued(str + i, res, neg, error));
}
