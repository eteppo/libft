# Introduction  
  
This project is about re-writing, understanding and learning to use the standard functions in C.  
  
## Part 1 - Libc functions  
  
Contains a set of the libc functions, as defined in their man but prefixed with "ft_"  
  
## Part 2 - Additional functions  
  
A set of functions that are either not included in the libc, or included in a different form.  
     

## Bonus part  
  
A set of functions to manipulate lists.  
  