NAME = libft.a
SRCS = 	ft_putendl_fd.c \
		ft_strmapi.c \
		ft_putchar_fd.c\
		ft_putnbr_fd.c \
		ft_putstr_fd.c \
		ft_itoa.c \
		ft_memchr.c \
		ft_atoi.c  \
		ft_atoi_with_error.c \
		ft_atof_with_error.c \
		ft_bzero.c \
		ft_isalnum.c \
		ft_isalpha.c \
		ft_isascii.c \
		ft_isdigit.c \
		ft_isprint.c \
		ft_is_whitespace.c \
		ft_is_in_set.c \
		ft_memccpy.c \
		ft_memcmp.c \
		ft_memcpy.c \
		ft_memmove.c \
		ft_memset.c \
		ft_strchr.c \
		ft_strcmp.c \
		ft_strlcpy.c \
		ft_strlen.c \
		ft_strncmp.c \
		ft_strnstr.c \
		ft_strrchr.c \
		ft_tolower.c \
		ft_toupper.c \
		ft_calloc.c  \
		ft_strdup.c  \
		ft_strndup.c \
		ft_strjoin.c \
		ft_strlcat.c \
		ft_substr.c  \
		ft_strtrim.c \
		ft_split.c \
		get_next_line.c
BONUS_SRCS_FILES =	ft_lstnew.c \
					ft_lstadd_front.c \
					ft_lstsize.c \
					ft_lstlast.c \
					ft_lstadd_back.c \
					ft_lstdelone.c \
					ft_lstclear.c \
					ft_lstiter.c \
					ft_lstmap.c
HEADER_FILES = libft.h
LIBC = ar -rcs
CFLAGS = -Wall -Wextra -Werror

ifdef WITH_BONUS
OBJ_FILES = $(SRCS:.c=.o) $(BONUS_SRCS_FILES:.c=.o)
else
OBJ_FILES = $(SRCS:.c=.o)
endif

all: $(NAME)

$(NAME): $(OBJ_FILES)
	$(LIBC) $(NAME) $(OBJ_FILES)

%.o: %.c $(HEADER_FILES)
	$(CC) -c $(CFLAGS) -o $@ $<

bonus:
	$(MAKE) WITH_BONUS=1 all

clean:
	rm -f $(SRCS:.c=.o) $(BONUS_SRCS_FILES:.c=.o) $(NAME)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean bonus re
