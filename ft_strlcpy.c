/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcpy.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/08/24 08:57:54 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/16 17:44:49 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dest, const char *src, size_t size)
{
	size_t	srclen;

	if (!dest && !src)
		return (0);
	srclen = ft_strlen(src);
	if (size > 0)
	{
		if (srclen + 1 < size)
			ft_memcpy(dest, src, srclen + 1);
		else
		{
			ft_memcpy(dest, src, size - 1);
			dest[size - 1] = '\0';
		}
	}
	return (srclen);
}
