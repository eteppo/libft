/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_putnbr_fd.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/11 11:04:15 by eteppo        #+#    #+#                 */
/*   Updated: 2020/11/16 16:00:21 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int nb, int fd)
{
	if (fd == -1)
		return ;
	if (nb > -2147483648)
	{
		if (nb < 0)
		{
			write(fd, "-", 1);
			ft_putnbr_fd(-1 * nb, fd);
		}
		else if (nb > 9)
		{
			ft_putnbr_fd(nb / 10, fd);
			ft_putnbr_fd(nb % 10, fd);
		}
		else
			ft_putchar_fd(nb + 48, fd);
	}
	else if (nb == -2147483648)
		write(fd, "-2147483648", 11);
}
