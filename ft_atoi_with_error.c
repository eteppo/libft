/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_atoi_with_error.c                               :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/15 12:11:23 by eteppo        #+#    #+#                 */
/*   Updated: 2022/01/15 14:04:39 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"  

static int	is_within_int_range(const char *str, long res, int sign, int *error)
{
	int	i;

	i = 0;
	*error = -1;
	if (sign == 1 && res * 10 + (str[i] - '0') > 2147483647)
		return (-1);
	else if (sign == -1 && res * 10 + (str[i] - '0') > 2147483648)
		return (0);
	*error = 0;
	return (1);
}

int	ft_atoi_with_error(char *str, int *error)
{
	long	result;
	int		sign;
	int		i;

	i = 0;
	result = 0;
	sign = 1;
	*error = 0;
	while (is_whitespace(str, i))
		i++;
	if (str[i] == '-')
		sign = -1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	while (ft_isdigit(str[i]) == 1)
	{
		if (is_within_int_range(str + i, result, sign, error) != 1)
			return (is_within_int_range(str + i, result, sign, error));
		result = result * 10 + (str[i] - '0');
		i++;
	}
	if ((result == 0 && ft_strcmp(str, "0\0") != 0) || str[i] != '\0')
		*error = -1;
	return (result * sign);
}
