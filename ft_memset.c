/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memset.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 10:31:47 by eteppo        #+#    #+#                 */
/*   Updated: 2020/11/21 17:41:57 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *str, int c, size_t n)
{
	size_t			i;
	unsigned char	value;

	value = c;
	i = 0;
	while (i < n)
	{
		((unsigned char *)str)[i] = value;
		i++;
	}
	return ((unsigned char *)str);
}
